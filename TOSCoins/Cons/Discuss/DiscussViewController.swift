//
//  DiscussViewController.swift
//  TOSCoins
//
//  Created by mac on 2021/3/17.
//  Copyright © 2021 Chacky. All rights reserved.
//

import UIKit
import MJRefresh

class DiscussViewController: TOSViewController {

    @IBOutlet weak var discussTable: UITableView!
    
    var discussModels = [DiscussModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.rebackBtn.isHidden = true
        
        discussTable.delegate = self
        discussTable.dataSource = self
        discussTable.register(UINib.init(nibName: "DiscussTableViewCell", bundle: nil), forCellReuseIdentifier: "cell")
        discussTable.mj_header = MJRefreshNormalHeader.init(refreshingTarget: self, refreshingAction: #selector(NSAGDDataInit))
        
        self.NSAGDDataInit()
        
        NotificationCenter.default.addObserver(self, selector: #selector(NSAGDDataInit), name: NSNotification.Name.init("modelChange"), object: nil)
        
        // Do any additional setup after loading the view.
    }
    
    
    @objc func NSAGDDataInit() {
        discussTable.mj_header?.endRefreshing()
        discussModels = TOSModel.shard.DiscussModel.arrayValue.map({ (json) in
            DiscussModel.init(json: json)
        })
        if discussModels.count >= 2{
            discussModels = discussModels.sorted(by: { (model1, model2) -> Bool in
                return model1.date >= model2.date
            })
        }
        self.discussTable.reloadData()
    }

    @objc func NSAGDReportBtnAct(sender: UIButton) {
        let cell = sender.superview?.superview?.superview?.superview?.superview as! DiscussTableViewCell
        NSAGDreportClick(viewCon: self, modelTag: cell.model!.tag)
    }
    
    @objc func NSAGDShieldBtnAct(sender: UIButton) {
        let cellModel = (sender.superview?.superview?.superview?.superview?.superview as! DiscussTableViewCell).model!
        TOSModel.deleteDiscussModel(json: discussModelToJson(model: cellModel))
    }
    
    @IBAction func NSAGDToPublishView(_ sender: UIButton) {
        NSAGDpushToSign(self)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}


extension DiscussViewController: UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return discussModels.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! DiscussTableViewCell
        cell.selectionStyle = .none
        cell.model = discussModels[indexPath.row]
        cell.NSAGDReportBtn.addTarget(self, action: #selector(NSAGDReportBtnAct(sender:)), for: .touchUpInside)
        cell.NSAGDShieldBtn.addTarget(self, action: #selector(NSAGDShieldBtnAct(sender:)), for: .touchUpInside)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
 
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let NSAGDNextView = NSAGDmainjStoryboard.instantiateViewController(withIdentifier: "DiscussDetailViewController") as! DiscussDetailViewController
        NSAGDNextView.model = discussModels[indexPath.row]
        self.navigationController?.pushViewController(NSAGDNextView, animated: true)
    }
    
}
