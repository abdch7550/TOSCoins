//
//  ReportViewController.swift
//  TOSCoins
//
//  Created by mac on 2021/3/17.
//  Copyright © 2021 Chacky. All rights reserved.
//

import UIKit

class ReportViewController: TOSViewController {

    
    @IBOutlet weak var reportTable: UITableView!
    @IBOutlet weak var reportBtn: UIButton!
    
    var modelTag = String()
    
    let reportContent = ["Post inappropriate content","Post scam content","Post infringing content","Publish pornography"]
    
    var selectTag: Int!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "Report"
        
        reportTable.delegate = self
        reportTable.dataSource = self
        reportTable.register(UINib.init(nibName: "ReportTableViewCell", bundle: nil), forCellReuseIdentifier: "cell")
        
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func reportBtnClick(_ sender: UIButton) {
        NSAGDsaveReports(modelTag: self.modelTag)
        NSAGDHUDHelper.showSuccess(withStatus: "Successful report, waiting for processing.")
        self.navigationController?.popViewController(animated: true)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension ReportViewController: UITableViewDelegate,UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! ReportTableViewCell
        cell.reportLabel.text = self.reportContent[indexPath.section]
        cell.layer.masksToBounds = true
        cell.layer.cornerRadius = 12
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let foot = UIView()
        foot.backgroundColor = .clear
        return foot
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 15
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.selectTag = indexPath.section
        self.reportBtn.isEnabled = true
        self.reportBtn.backgroundColor = .systemBlue
    }
}
