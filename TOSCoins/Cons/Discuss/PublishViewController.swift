//
//  PublishViewController.swift
//  TOSCoins
//
//  Created by mac on 2021/3/17.
//  Copyright © 2021 Chacky. All rights reserved.
//

import UIKit
import Photos

class PublishViewController: TOSViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate{

    @IBOutlet weak var titleTextView: UITextView!
    @IBOutlet weak var contentTextView: UITextView!
    @IBOutlet weak var imageBtn: UIButton!
    
    var imagePicker = UIImagePickerController.init()
    var imageName = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //imagePick
        imagePicker.delegate = self
        imagePicker.allowsEditing  = true
        // Do any additional setup after loading the view.
    }
    

    @IBAction func ImageBtnClick(_ sender: UIButton) {
        let imageAlert = UIAlertController.init(title: nil, message: nil, preferredStyle: .alert)

//        let camera = UIAlertAction.init(title: "Camera", style: .default) { (UIAlertAction) in
//
//            if UIImagePickerController.isSourceTypeAvailable(.camera){
//                let cStatus = AVCaptureDevice.authorizationStatus(for: .video)
//                switch cStatus {
//
//                case .notDetermined:
//                    AVCaptureDevice.requestAccess(for: .video) { (bool) in
//                        if bool{
//                            self.presentCamera()
//                        }
//                    }
//                case .restricted:
//                    AVCaptureDevice.requestAccess(for: .video) { (bool) in
//                        if bool{
//                            self.presentCamera()
//                        }
//                    }
//                case .denied:
//                    AVCaptureDevice.requestAccess(for: .video) { (bool) in
//                        if bool{
//                            self.presentCamera()
//                        }
//                    }
//                case .authorized:
//                    self.presentCamera()
//                @unknown default:
//                    break
//                }
//            }
//            else{
//                NSAGDHUDHelper.showError(withStatus: "Camera unavailable.")
//            }
//
//        }

        let album = UIAlertAction.init(title: "Album", style: .default) { (UIAlertAction) in
            if UIImagePickerController.isSourceTypeAvailable(.photoLibrary){
                let pStatus = PHPhotoLibrary.authorizationStatus()
                switch pStatus {
                case .notDetermined:
                    PHPhotoLibrary.requestAuthorization { (status) in
                        if status == .authorized{
                            self.presentAlbum()
                        }
                    }
                case .restricted:
                    PHPhotoLibrary.requestAuthorization { (status) in
                        if status == .authorized{
                            self.presentAlbum()
                        }
                    }
                case .denied:
                    PHPhotoLibrary.requestAuthorization { (status) in
                        if status == .authorized{
                            self.presentAlbum()
                        }
                    }
                case .authorized:
                    self.presentAlbum()
                @unknown default:
                    break
                }
            }
            else{
                NSAGDHUDHelper.showError(withStatus: "Album unavailable.")
            }
        }

        let cancel = UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil)

//        imageAlert.addAction(camera)
        imageAlert.addAction(album)
        imageAlert.addAction(cancel)

        self.present(imageAlert, animated: true, completion: nil)
    }

    func presentCamera(){
        DispatchQueue.main.async {
            self.imagePicker.sourceType = .camera
            self.imagePicker.modalPresentationStyle = .fullScreen
            self.present(self.imagePicker, animated: true, completion: nil)
        }
    }

    func presentAlbum(){
        DispatchQueue.main.async {
            self.imagePicker.sourceType = .photoLibrary
            self.imagePicker.modalPresentationStyle = .fullScreen
            self.present(self.imagePicker, animated: true, completion: nil)
        }
    }
    
    @IBAction func publishBtnClick(_ sender: UIButton) {
        if self.contentTextView.text.isEmpty || imageName.isEmpty || self.titleTextView.text.isEmpty{
            NSAGDHUDHelper.showInfo(withStatus: "Please edit the published content.")
        }
        else{
            let NSAGDdic = DiscussModel()//[String:Any]()
            let dateStr = String(Int(Date().timeIntervalSince1970))
            NSAGDdic.tag = dateStr
            NSAGDdic.nick = NSAGDuserStand.string(forKey: NSAGDuserNameKey)!
            NSAGDdic.date = NSAGDmyDateFormat.string(from: Date())
            NSAGDdic.title = self.titleTextView.text
            NSAGDdic.content = self.contentTextView.text
            NSAGDdic.image = self.imageName
            NSAGDdic.portrait = NSAGDuserStand.string(forKey: NSAGDuserPortraitKey)!
            NSAGDdic.userID = NSAGDuserStand.string(forKey: NSAGDuserIDKey)!
            NSAGDdic.comments = [CommunityCommentModel.init()]
            TOSModel.changeDiscussModel(json: discussModelToJson(model: NSAGDdic))
            NSAGDHUDHelper.showSuccess(withStatus: "Successfully published !")
            self.navigationController?.popViewController(animated: true)
        }
        
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {

        let image = info[.originalImage] as! UIImage

        let name = String(Int(Date().timeIntervalSince1970))+".png"
        self.imageName = name

        self.imageBtn.setImage(UIImage(), for: .normal)
        self.imageBtn.setBackgroundImage(image, for: .normal)

        let _ = NSAGDsaveImage(currentImage: self.imageBtn.backgroundImage(for: .normal)!, persent: 1, imageName: self.imageName)

         self.imagePicker.dismiss(animated: true, completion: nil)
    }
    
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
