//
//  DiscussDetailViewController.swift
//  TOSCoins
//
//  Created by mac on 2021/3/17.
//  Copyright © 2021 Chacky. All rights reserved.
//

import UIKit

class DiscussDetailViewController: TOSViewController {

    var model: DiscussModel!
    
    @IBOutlet weak var NSAGDImage: UIImageView!
    @IBOutlet weak var NSAGDTitle: UILabel!
    @IBOutlet weak var NSAGDPortrait: UIImageView!
    @IBOutlet weak var NSAGDNick: UILabel!
    @IBOutlet weak var NSAGDDate: UILabel!
    @IBOutlet weak var NSAGDContent: UITextView!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.NSAGDDataInit()
        
        // Do any additional setup after loading the view.
    }
    
    func NSAGDDataInit() {
        model = TOSModel.shard.DiscussModel.arrayValue.map({ (json) in
            DiscussModel.init(json: json)
        }).filter({ (mod) -> Bool in
            return model.tag == mod.tag
        })[0]
        
        let image = UIImage.init(named: model.image)
        if image != nil{
            NSAGDImage.image = image
        }
        else{
            NSAGDImage.image = NSAGDgetDiscussImage(imagePath: model.image)
        }
        
        NSAGDTitle.text = model.title
        let portrait = UIImage.init(named: model.portrait)
        if portrait != nil{
            NSAGDPortrait.image = portrait
        }
        else{
            NSAGDPortrait.image = NSAGDgetDiscussImage(imagePath: model.portrait)
        }
        
        NSAGDNick.text = model.nick
        NSAGDDate.text = model.date
        
        NSAGDContent.text = model.content
    }
    
    
    @IBAction func NSAGDReportBtnAct(_ sender: UIButton) {
        NSAGDreportClick(viewCon: self, modelTag: model.tag)
    }
    
    @IBAction func NSAGDShieldBtnAct(_ sender: UIButton) {
        TOSModel.deleteDiscussModel(json: discussModelToJson(model: model))
        self.navigationController?.popViewController(animated: true)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
