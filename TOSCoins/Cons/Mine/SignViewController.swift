//
//  SignViewController.swift
//  TOSCoins
//
//  Created by mac on 2021/3/17.
//  Copyright © 2021 Chacky. All rights reserved.
//

import UIKit

class SigninViewController: TOSViewController {
    
    @IBOutlet var infoViews: [UIView]!
    @IBOutlet weak var reBtn: UIButton!
    @IBOutlet weak var accountField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    @IBOutlet weak var passwordTip: UILabel!
    
    @IBOutlet weak var forgetBTn: UIButton!
    
    var isSignIn = true
    
    override func viewDidLoad() {
        super.viewDidLoad()


        reBtn.layer.masksToBounds = true
        reBtn.layer.borderColor = NSAGDUIColorFromRGB(color_vaule: 0x4B27AB).cgColor
        reBtn.layer.borderWidth = 0.5
        
        accountField.attributedPlaceholder = NSAttributedString.init(string: "Account", attributes: [NSAttributedString.Key.foregroundColor : UIColor.white])
        passwordField.attributedPlaceholder = NSAttributedString.init(string: "Password", attributes: [NSAttributedString.Key.foregroundColor : UIColor.white])
        
        self.viewchange()
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func forgetClick(_ sender: UIButton) {
        if accountField.text!.isEmpty{
            NSAGDHUDHelper.showError(withStatus: "Please input the account first")
        }
        else{
            let userInfos = NSAGDuserStand.array(forKey: NSAGDuserInfosKey) as! [[String:String]]
            print("userInfos:\(userInfos)")
            let userInfoMid = userInfos.filter({ (NSAGDdic) -> Bool in
                return (NSAGDdic[NSAGDuserNameKey] == accountField.text)
            })
            if userInfoMid.count == 0 {
                NSAGDHUDHelper.showInfo(withStatus: "The account does not exist ~")
            }
            else{
                let userInfo = userInfoMid[0]
                self.passwordTip.text = (userInfo[NSAGDuserPasswordKey]!.prefix(3)) + "****"
                self.passwordTip.isHidden = false
            }
        }
    }
    
    @IBAction func logInCLick(_ sender: UIButton) {
        
        if accountField.text!.isEmpty || passwordField.text!.isEmpty {
            NSAGDHUDHelper.showError(withStatus: "Account or password cannot be empty")
        }
        else if !NSAGDisReachability(){
            NSAGDHUDHelper.showError(withStatus: "Network error")
        }
        else if sender.isSelected{//zhuce
            isSignIn = true
            var userInfos = NSAGDuserStand.array(forKey: NSAGDuserInfosKey) as! [[String:String]]
            var NSAGDdic = [String:String]()
            NSAGDdic[NSAGDuserNameKey] = accountField.text
            NSAGDdic[NSAGDuserPortraitKey] = "UP"
            NSAGDdic[NSAGDuserIDKey] = String(Int(Date().timeIntervalSince1970))
            NSAGDdic[NSAGDuserPasswordKey] = passwordField.text
            
            userInfos.append(NSAGDdic)
            
            NSAGDuserStand.set(userInfos, forKey: NSAGDuserInfosKey)
            self.viewchange()
            NSAGDHUDHelper.showSuccess(withStatus: "Sing up successful, please Login")
        }
        else{
            
            let userInfos = NSAGDuserStand.array(forKey: NSAGDuserInfosKey) as! [[String:String]]
            for info in userInfos{
                if accountField.text != info[NSAGDuserNameKey] || passwordField.text != info[NSAGDuserPasswordKey]{
                    NSAGDHUDHelper.showError(withStatus: "Wrong account or password")
                }
                else{
                    NSAGDHUDHelper.showSuccess(withStatus: "Sign in successful")
                    NSAGDuserStand.set(true, forKey: NSAGDuserStateKey)
                    NSAGDuserStand.set(accountField.text, forKey: NSAGDuserNameKey)
                    NSAGDuserStand.set(passwordField.text, forKey: NSAGDuserPasswordKey)
                    NSAGDuserStand.set(info[NSAGDuserIDKey], forKey: NSAGDuserIDKey)
                    NSAGDuserStand.set(info[NSAGDuserPortraitKey], forKey: NSAGDuserPortraitKey)
                    NotificationCenter.default.post(name: NSNotification.Name.init("stateChange"), object: nil)
                    self.navigationController?.popViewController(animated: true)
                    break
                }
            }
        }
    }
    
    @IBAction func register(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        isSignIn = !sender.isSelected
        self.viewchange()
    }
    
    func viewchange(){
        passwordTip.isHidden = true
        accountField.text = ""
        passwordField.text = ""
        
        self.title = isSignIn ? "Sign in" : "Sign up"
        
        (infoViews[2] as! UIButton).isSelected = !isSignIn
        (infoViews[3] as! UIButton).isSelected = !isSignIn
        forgetBTn.isHidden = !isSignIn
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
