//
//  MineViewController.swift
//  TOSCoins
//
//  Created by mac on 2021/3/17.
//  Copyright © 2021 Chacky. All rights reserved.
//

import UIKit

class MineViewController: TOSViewController {

    @IBOutlet weak var signinBtn: UIButton!
    @IBOutlet weak var signoutBtn: UIButton!
    
    @IBOutlet weak var userPortrait: UIImageView!
    @IBOutlet weak var userNick: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.rebackBtn.isHidden = true
        self.viewInit()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        viewInit()
    }
    
    func viewInit() {
        if NSAGDuserStand.bool(forKey: NSAGDuserStateKey) {
            userPortrait.image = NSAGDgetPortraitImage(imagePath: NSAGDuserStand.string(forKey: NSAGDuserPortraitKey)!)
            userNick.text = NSAGDuserStand.string(forKey: NSAGDuserNameKey)
            signinBtn.isEnabled = false
            signoutBtn.isHidden = false
        }
        else {
            userPortrait.image = UIImage.init(named: "注册流程")
            userNick.text = "Sign in"
            signinBtn.isEnabled = true
            signoutBtn.isHidden = true
        }
    }
    
    @IBAction func NSAGDClear(_ sender: Any) {
            let alertController = UIAlertController.init(title: nil, message: "Are you sure to clear the cache?", preferredStyle: .alert)
            let sureBtn = UIAlertAction.init(title: "Sure", style: .default) { (_) in
                let fileManager = FileManager.default
                let path = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
                let fileArray = fileManager.subpaths(atPath: path)
                var subSize : UInt64 = 0
                
                for item in fileArray!{
                    
                    let attr = try? fileManager.attributesOfItem(atPath: path + "/" + item)
                    let size = attr![FileAttributeKey.size] as! UInt64
                    subSize += size
                    try? fileManager.removeItem(atPath: path + "/" + item)
                }
                let sizeM = Double(subSize/1048576)
                NSAGDHUDHelper.showInfo(withStatus: "The cache has been cleaned up, a total of cleared" + String.init(format: "%.1f M", sizeM))
                NSAGDHUDHelper.setMaskType(type: .black)
            }
            let cancelBtn = UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil)
            alertController.addAction(sureBtn)
            alertController.addAction(cancelBtn)
            self.present(alertController, animated: true, completion: nil)
    }
    
    @IBAction func toFavorites(_ sender: UITapGestureRecognizer) {
        if NSAGDuserStand.bool(forKey: NSAGDuserStateKey) {
            let NSAGDNext = NSAGDmainjStoryboard.instantiateViewController(withIdentifier: "FavoritesViewController") as! FavoritesViewController
            self.navigationController?.pushViewController(NSAGDNext, animated: true)
        }
        else {
            NSAGDpushToSign(self)
        }
    }
    
    @IBAction func signoutClick(_ sender: UIButton) {
        let NSAGDAlert = UIAlertController.init(title: "", message: "Sign out?", preferredStyle: .alert)
        let NSAGDNo = UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil)
        let NSAGDYes = UIAlertAction.init(title: "Confirm", style: .default) { (UIAlertAction) in
            NSAGDuserStand.set(false, forKey: NSAGDuserStateKey)
            self.viewInit()
        }
        NSAGDAlert.addAction(NSAGDNo)
        NSAGDAlert.addAction(NSAGDYes)
        self.present(NSAGDAlert, animated: true, completion: nil)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
