//
//  FeedbackViewController.swift
//  TOSCoins
//
//  Created by mac on 2021/3/17.
//  Copyright © 2021 Chacky. All rights reserved.
//

import UIKit

class FeedbackViewController: TOSViewController {

    @IBOutlet weak var whiteBGView: UIView!
    @IBOutlet weak var feedTextView: UITextView!
    @IBOutlet weak var sendBtn: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()

        feedTextView.clipsToBounds = true
        feedTextView.layer.cornerRadius = 12
        sendBtn.layer.masksToBounds = true
        sendBtn.layer.cornerRadius = 12
        
        whiteBGView.layer.masksToBounds = true
        whiteBGView.layer.cornerRadius = 30
        
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func toSend(_ sender: UIButton) {
        let alert = UIAlertController.init(title: "", message: "Network not connected", preferredStyle: .alert)
        let cancel = UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil)
        let confirm = UIAlertAction.init(title: "Confirm", style: .default) { (defaults) in

            self.navigationController?.popViewController(animated: true)
        }
        if feedTextView.text.isEmpty{
            alert.title = ""
            alert.message = "Please enter feedback"
            alert.addAction(cancel)
            self.present(alert, animated: true, completion: nil)
        }
        else if !NSAGDisReachability(){
            alert.title = ""
            alert.message = "Network not connected"
            alert.addAction(cancel)
            self.present(alert, animated: true, completion: nil)
        }else{
            alert.title = ""
            alert.message = "The feedback message was sent successfully!"
            alert.addAction(confirm)
            self.present(alert, animated: true, completion: nil)
        }
    }

    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
