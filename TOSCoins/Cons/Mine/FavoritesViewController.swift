//
//  FavoritesViewController.swift
//  TOSCoins
//
//  Created by mac on 2021/3/17.
//  Copyright © 2021 Chacky. All rights reserved.
//

import UIKit
import MJRefresh

class FavoritesViewController: TOSViewController {

    @IBOutlet weak var NSAGDFavoritesTable: UITableView!
    var tableModels = [BTCModel]()
    override func viewDidLoad() {
        super.viewDidLoad()

        
        NSAGDFavoritesTable.delegate = self
        NSAGDFavoritesTable.dataSource = self
        NSAGDFavoritesTable.register(UINib.init(nibName: "QuotesTableViewCell", bundle: nil), forCellReuseIdentifier: "cell")
        NSAGDFavoritesTable.mj_header = MJRefreshNormalHeader.init(refreshingTarget: self, refreshingAction: #selector(coinDataInit))
        
        
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.coinDataInit()
    }

    @objc func coinDataInit() {
        let favortes = NSAGDgetMyCollects()
        if favortes.count != 0{
            NetWorkingNSAGD.BTCRequest(requestType: .get, parm: ["type":"price","names":favortes]) { (bool, value) in
                if bool{
                    let coinModels = (value as! [JSON]).map({ (json) in
                        BTCModel.init(json: json)
                    })
                    self.tableModels = coinModels
                }
                else{
                    NSAGDHUDHelper.showString(withStatus: value as? String)
                }
            }
        }
        else{
            tableModels = [BTCModel]()
        }
        self.NSAGDFavoritesTable.reloadData()
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension FavoritesViewController: UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableModels.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! QuotesTableViewCell
        cell.selectionStyle = .none
        cell.model = tableModels[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 56
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let nextV = NSAGDmainjStoryboard.instantiateViewController(withIdentifier: "QuotesDetailViewController") as! QuotesDetailViewController
        nextV.model = tableModels[indexPath.row]
        self.navigationController?.pushViewController(nextV, animated: true)
    }
}
