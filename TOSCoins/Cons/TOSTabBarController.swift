//
//  PCTabBarController.swift
//  TOSCoins
//
//  Created by mac on 2021/3/17.
//  Copyright © 2021 Chacky. All rights reserved.
//

import UIKit

class TOSTabBarController: UITabBarController,UITabBarControllerDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()

        let items = self.tabBar.items
        for item in items!{
            item.setTitleTextAttributes([NSAttributedString.Key.foregroundColor : UIColor.darkGray], for: .selected)
        }
        
        self.firstOpen()
        
        // Do any additional setup after loading the view.
    }
    
    func firstOpen(){
        if !NSAGDuserStand.bool(forKey: "TOSCoins"){
            NSAGDuserStand.set(true, forKey: "TOSCoins")
            NSAGDuserStand.set(NSAGDuserInfosValue, forKey: NSAGDuserInfosKey)
            
             //model
             let path = Bundle.main.path(forResource: "Discuss", ofType: "plist")
             let modelarray = NSMutableDictionary.init(contentsOfFile: path!)?.allValues as! [[String:Any]]
             for model in modelarray{
                 TOSModel.changeDiscussModel(json: JSON(model))
             }
        }
    }
    
    override func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        print("select")
        let items = self.tabBar.items
        for item in items!{
            item.setTitleTextAttributes([NSAttributedString.Key.foregroundColor : UIColor.darkGray], for: .selected)
        }
    }
    
   
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
