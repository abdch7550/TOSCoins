//
//  NewsDetailViewController.swift
//  TOSCoins
//
//  Created by mac on 2021/3/17.
//  Copyright © 2021 Chacky. All rights reserved.
//

import UIKit
import WebKit

class NewsDetailViewController: TOSViewController {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    var model = NewsModel()
    
    @IBOutlet weak var newsWK: WKWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        titleLabel.text = model.title
        dateLabel.text = model.date

        newsWK.scrollView.backgroundColor = .clear
        newsWK.backgroundColor = .clear
        newsWK.navigationDelegate = self
        newsWK.scrollView.showsHorizontalScrollIndicator = false
        newsWK.scrollView.showsVerticalScrollIndicator = false

        let NSAGDmainWidthStr = String(Int(UIScreen.main.bounds.width))+"px"
        
        var contentString = model.content
        
        contentString = String.init(format: "<html><head><meta name='viewport' content='width=\(NSAGDmainWidthStr);initial-scale=1.0;maximum-scale=1.0;minimum-scale=1.0;user-scalable=no;'><font color=\"black\"><style>img{max-width:\(NSAGDmainWidthStr);}</style><style type=\"text/css\">body{margin: 0px;padding: 5px;}#Content a:link {color:#000000;text-decoration:none;}#Content a:visiteid,#Content a:hover,#Content a:active,</style></head><body><div><div id=\"Content\">%@</div></div></body></html>", contentString)
        
        NSAGDHUDHelper.setMaskType(type: .clear)
        
        NSAGDHUDHelper.show()
        
        newsWK.loadHTMLString(contentString, baseURL: nil)
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = false
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension NewsDetailViewController:WKNavigationDelegate{
    
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        if (navigationAction.request.url?.scheme?.contains("http"))!{
            decisionHandler(WKNavigationActionPolicy.cancel)
        }
        else{
            decisionHandler(WKNavigationActionPolicy.allow)
        }
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        webView.evaluateJavaScript("document.getElementsByTagName('body')[0].style.webkitTextSizeAdjust= '100%'", completionHandler: nil)
        NSAGDHUDHelper.dismiss()
    }
}
