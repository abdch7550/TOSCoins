//
//  NewsViewController.swift
//  TOSCoins
//
//  Created by mac on 2021/3/17.
//  Copyright © 2021 Chacky. All rights reserved.
//

import UIKit
import MJRefresh

class NewsViewController: TOSViewController {

    
    @IBOutlet weak var coinCollect: UICollectionView!
    var coinModels = [BTCModel]()
    
    @IBOutlet weak var newsTable: UITableView!
    var newsModels = [NewsModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.rebackBtn.isHidden = true
        
        let layout = UICollectionViewFlowLayout()
        layout.itemSize = CGSize.init(width: (NSAGDmainWidth-20-30)/3, height: 128)
        layout.scrollDirection = .vertical
        layout.minimumInteritemSpacing = 10
        layout.sectionInset = UIEdgeInsets.init(top: 0, left: 0, bottom: 0, right: 0)
        
        coinCollect.collectionViewLayout = layout
        coinCollect.delegate = self
        coinCollect.dataSource = self
        coinCollect.register(UINib.init(nibName: "QuotesCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "cell")
        
        newsTable.delegate = self
        newsTable.dataSource = self
        newsTable.register(UINib.init(nibName: "NewsTableViewCell", bundle: nil), forCellReuseIdentifier: "cell")
        newsTable.mj_header = MJRefreshNormalHeader.init(refreshingTarget: self, refreshingAction: #selector(dataInit))
        
        self.dataInit()
        // Do any additional setup after loading the view.
    }
    
    
    @objc func dataInit() {
        newsTable.mj_header?.endRefreshing()
        self.coinDataInit()
        self.newsDataInit()
    }
    
    func coinDataInit() {
        NetWorkingNSAGD.BTCRequest(requestType: .get, parm: ["type":"price","names":NSAGDCoins]) { (bool, value) in
            if bool{
                self.coinModels = Array((value as! [JSON]).map({ (json) in
                    BTCModel.init(json: json)
                })[0...2])
                self.coinCollect.reloadData()
            }
            else{
                NSAGDHUDHelper.showString(withStatus: value as? String)
            }
        }
    }
    
    func newsDataInit() {
        NetWorkingNSAGD.BTCRequest(requestType: .get, parm: ["type":"noNormal","size":"23"]) { (bool, value) in
            if bool{
                self.newsModels = (value as! [JSON]).map({ (json) in
                    NewsModel.init(json: json)
                })
                self.newsTable.reloadData()
            }
            else{
                NSAGDHUDHelper.showString(withStatus: value as? String)
            }
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}


extension NewsViewController: UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return newsModels.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! NewsTableViewCell
        cell.selectionStyle = .none
        cell.model = newsModels[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let NSAGDNextView = NSAGDmainjStoryboard.instantiateViewController(withIdentifier: "NewsDetailViewController") as! NewsDetailViewController
        NSAGDNextView.model = newsModels[indexPath.row]
        self.navigationController?.pushViewController(NSAGDNextView, animated: true)
    }
}

extension NewsViewController: UICollectionViewDelegate,UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return coinModels.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! QuotesCollectionViewCell
        cell.model = coinModels[indexPath.row]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let nextV = NSAGDmainjStoryboard.instantiateViewController(withIdentifier: "QuotesDetailViewController") as! QuotesDetailViewController
        nextV.model = coinModels[indexPath.row]
        self.navigationController?.pushViewController(nextV, animated: true)
    }
}
