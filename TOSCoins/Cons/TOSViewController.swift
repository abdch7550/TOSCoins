//
//  TOSViewController.swift
//  TOSCoins
//
//  Created by mac on 2021/3/17.
//  Copyright © 2021 Chacky. All rights reserved.
//

import UIKit

class TOSViewController: UIViewController {

    let rebackBtn = UIButton.init()
    
//    var tabBarHidden:Bool!{
//        didSet{
//            (self.tabBarController as! SCTabBarController).tabBarView.isHidden = tabBarHidden
//        }
//    }
    
    var navibarIsTranslucent:Bool!{
        didSet{
            let navi = self.navigationController?.navigationBar
            navi?.shadowImage = UIImage()
//            navi?.setBackgroundImage(UIImage(), for: .default)
//            navi?.isTranslucent = navibarIsTranslucent
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.view.backgroundColor = NSAGDUIColorFromRGB(color_vaule: 0xEEEEEE)
        let naviView = UIView.init()
        view.addSubview(naviView)
        view.sendSubviewToBack(naviView)
        naviView.anchor(top: view.safeAreaLayoutGuide.topAnchor, bottom: nil, left: view.leftAnchor, right: view.rightAnchor, paddingTop: 0, paddingBottom: 0, paddingLeft: 0, paddingRight: 0, width: NSAGDmainWidth, height: 20)
        naviView.backgroundColor = NSAGDUIColorFromRGB(color_vaule: 0x2B475F)
        
        
        let image = UIImageView.init(image: UIImage.init(named: "Image"))
        image.frame = self.view.bounds
        image.contentMode = .scaleAspectFill
        self.view.addSubview(image)
        self.view.sendSubviewToBack(image)
        
        //tabBarHidden = true
        navibarIsTranslucent = true
        
        rebackBtn.setImage(UIImage.init(named: "left")?.withRenderingMode(.alwaysOriginal), for: .normal)
        rebackBtn.addTarget(self, action: #selector(turnback(sender:)), for: .touchUpInside)
        
        self.navigationItem.setLeftBarButton(UIBarButtonItem.init(customView: rebackBtn), animated: false)
        // Do any additional setup after loading the view.
    }
    
    @objc func turnback(sender:UIButton){
        self.navigationController?.popViewController(animated: true)
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
