//
//  QuotesViewController.swift
//  TOSCoins
//
//  Created by mac on 2021/3/17.
//  Copyright © 2021 Chacky. All rights reserved.
//

import UIKit
import MJRefresh
class QuotesViewController: TOSViewController {

    @IBOutlet weak var quoteCollect: UICollectionView!
    @IBOutlet weak var quoteTable: UITableView!
    
    var collectModels = [BTCModel]()
    var tableModels = [BTCModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.rebackBtn.isHidden = true
        
        let layout = UICollectionViewFlowLayout()
        layout.itemSize = CGSize.init(width: (NSAGDmainWidth-60-20)/3, height: 128)
        layout.scrollDirection = .vertical
        layout.minimumInteritemSpacing = 10
        layout.sectionInset = UIEdgeInsets.init(top: 0, left: 0, bottom: 0, right: 0)
        
        quoteCollect.collectionViewLayout = layout
        quoteCollect.delegate = self
        quoteCollect.dataSource = self
        quoteCollect.register(UINib.init(nibName: "QuotesCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "cell")
        
        quoteTable.delegate = self
        quoteTable.dataSource = self
        quoteTable.register(UINib.init(nibName: "QuotesTableViewCell", bundle: nil), forCellReuseIdentifier: "cell")
        quoteTable.mj_header = MJRefreshNormalHeader.init(refreshingTarget: self, refreshingAction: #selector(coinDataInit))
        
        self.coinDataInit()
        // Do any additional setup after loading the view.
    }
    

    @objc func coinDataInit() {
        NetWorkingNSAGD.BTCRequest(requestType: .get, parm: ["type":"price","names":NSAGDCoins]) { (bool, value) in
            if bool{
                let coinModels = (value as! [JSON]).map({ (json) in
                    BTCModel.init(json: json)
                })
                
                self.collectModels = Array(coinModels.sorted(by: { (model1, model2) -> Bool in
                    return model1.price >= model2.price
                })[0...2])
                
                self.tableModels = coinModels
                
                self.quoteCollect.reloadData()
                self.quoteTable.reloadData()
            }
            else{
                NSAGDHUDHelper.showString(withStatus: value as? String)
            }
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension QuotesViewController: UICollectionViewDelegate,UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return collectModels.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! QuotesCollectionViewCell
        cell.model = collectModels[indexPath.row]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let nextV = NSAGDmainjStoryboard.instantiateViewController(withIdentifier: "QuotesDetailViewController") as! QuotesDetailViewController
        nextV.model = collectModels[indexPath.row]
        self.navigationController?.pushViewController(nextV, animated: true)
    }
}

extension QuotesViewController: UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableModels.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! QuotesTableViewCell
        cell.selectionStyle = .none
        cell.model = tableModels[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 56
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let nextV = NSAGDmainjStoryboard.instantiateViewController(withIdentifier: "QuotesDetailViewController") as! QuotesDetailViewController
        nextV.model = tableModels[indexPath.row]
        self.navigationController?.pushViewController(nextV, animated: true)
    }
}
