//
//  QuotesDetailViewController.swift
//  TOSCoins
//
//  Created by mac on 2021/3/17.
//  Copyright © 2021 Chacky. All rights reserved.
//

import UIKit
import Charts

class QuotesDetailViewController: TOSViewController {

    var model: BTCModel!
    var historyModels = [HistoryModel]()
    
    @IBOutlet weak var NSAGDImage: UIImageView!
    @IBOutlet weak var NSAGDSingleName: UILabel!
    @IBOutlet weak var NSAGDPrice: UILabel!
    @IBOutlet weak var NSAGDRange: UILabel!
    
    @IBOutlet weak var NSAGDTotal: UILabel!
    @IBOutlet weak var NSAGDHighest: UILabel!
    @IBOutlet weak var NSAGDLowest: UILabel!
    
    @IBOutlet weak var NSAGDLineChart: LineChartView!
    
    @IBOutlet weak var NSAGDLikeBtn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.tnkjDataViewInit()
        
        // Do any additional setup after loading the view.
    }
    
    func tnkjDataViewInit() {
        NSAGDImage.sd_setImage(with: URL.init(string: NSAGDbitCoinsImages[model.name]!)) { (UIImage1, Error1, SDImageCacheType1, URL1) in
            self.NSAGDGetHistoryData()
        }
        NSAGDSingleName.text = model.singleName
        
        let range = model.range
        let color = range>=0 ? NSAGDupColor : NSAGDdownColor
        let rangePre = range>=0 ? "+" : ""
        
        NSAGDPrice.text = String.init(format: "$%.2f", model.price)
        NSAGDPrice.textColor = color
        NSAGDRange.text = rangePre + String.init(format: "%.2f", range) + "%"
        NSAGDRange.textColor = color
        
        NSAGDTotal.text = String.init(format: "$%.2f", model.totalPrice)
        NSAGDHighest.text = String.init(format: "$%.2f", model.highest)
        NSAGDLowest.text = String.init(format: "$%.2f", model.lowest)
        
        self.NSAGDLikeBtn.isSelected = NSAGDgetMyCollects().contains(model.name)
    }
    
    func NSAGDGetHistoryData() {
        NetWorkingNSAGD.BTCRequest(parm: ["type":"history","coin":model.name,"days":1,"interval":"1h"]) { (bool, value) in
            if bool{
                let jsons = value as! [JSON]
                self.historyModels = [HistoryModel]()
                for json in jsons{
                    self.historyModels.append(HistoryModel.init(json: json))
                }
                self.NSAGDChartViewInit()
            }
            else{
                NSAGDHUDHelper.showError(withStatus: value as? String)
            }
        }
    }
    
    func NSAGDChartViewInit() {
        let color = NSAGDImage.image!.sd_color(at: CGPoint.init(x: (NSAGDImage.image?.size.width)!/4, y: (NSAGDImage.image?.size.height)!/2))!
        
        NSAGDLineChart.legend.formSize = 2
        
        NSAGDLineChart.noDataText = "No Data."
        NSAGDLineChart.noDataTextColor = .white
        
        let XLine = NSAGDLineChart.xAxis
        XLine.axisLineColor = .clear
        XLine.drawGridLinesEnabled = true
        XLine.gridColor = color
        
        XLine.labelTextColor = .clear
        
        let YLine = NSAGDLineChart.leftAxis
        //YLine.axisLineColor = .clear
        YLine.drawGridLinesEnabled = false
        YLine.gridColor = UIColor.init(patternImage: NSAGDImage.image!)//NSAGDUIColorFromRGB(color_vaule: 0x2470A5)
        YLine.labelTextColor = .white
        
        if historyModels.count != 0{
            var dataEntries = [ChartDataEntry]()
            for model in historyModels {
                let entry = ChartDataEntry.init(x: Double(model.timeInterval), y: model.price)
                dataEntries.append(entry)
            }
            
            let lineChartSet = LineChartDataSet.init(entries: dataEntries)
            lineChartSet.mode = .horizontalBezier
            //lineChartSet.drawValuesEnabled = false
            lineChartSet.valueColors = [.white]
            //lineChartSet.drawFilledEnabled = true
            
            color.withAlphaComponent(0.5)
            //lineChartSet.fillColor = color
            lineChartSet.circleColors = [.clear]
            //lineChartSet.drawCircleHoleEnabled = false
            lineChartSet.circleHoleColor = color
            lineChartSet.colors = [UIColor.init(patternImage: NSAGDImage.image!)]
            let lineCharData = LineChartData.init(dataSets: [lineChartSet])
            
            NSAGDLineChart.data = lineCharData
        }
    }
    
    @IBAction func NSAGDLikeBtnClick(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        var myCollects = NSAGDgetMyCollects()
        if sender.isSelected {
            myCollects.append(model.name)
        }
        else {
            myCollects.remove(at: myCollects.firstIndex(of: model.name)!)
        }
        saveMyCollects(collects: myCollects)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
