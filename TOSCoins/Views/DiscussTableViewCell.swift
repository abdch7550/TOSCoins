//
//  DiscussTableViewCell.swift
//  TOSCoins
//
//  Created by mac on 2021/3/17.
//  Copyright © 2021 Chacky. All rights reserved.
//

import UIKit

class DiscussTableViewCell: UITableViewCell {

    @IBOutlet weak var NSAGDImage: UIImageView!
    @IBOutlet weak var NSAGDTitle: UILabel!
    @IBOutlet weak var NSAGDPortrait: UIImageView!
    @IBOutlet weak var NSAGDNick: UILabel!
    @IBOutlet weak var NSAGDDate: UILabel!
    @IBOutlet weak var NSAGDReportBtn: UIButton!
    @IBOutlet weak var NSAGDShieldBtn: UIButton!
    
    var model: DiscussModel! {
        didSet{
            let image = UIImage.init(named: model.image)
            if image != nil{
                NSAGDImage.image = image
            }
            else{
                NSAGDImage.image = NSAGDgetDiscussImage(imagePath: model.image)
            }
            
            NSAGDTitle.text = model.title
            let portrait = UIImage.init(named: model.portrait)
            if portrait != nil{
                NSAGDPortrait.image = portrait
            }
            else{
                NSAGDPortrait.image = NSAGDgetDiscussImage(imagePath: model.portrait)
            }
            
            NSAGDNick.text = model.nick
            NSAGDDate.text = model.date
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
