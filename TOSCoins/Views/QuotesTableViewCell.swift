//
//  QuotesTableViewCell.swift
//  TOSCoins
//
//  Created by mac on 2021/3/17.
//  Copyright © 2021 Chacky. All rights reserved.
//

import UIKit

class QuotesTableViewCell: UITableViewCell {

    @IBOutlet weak var NSAGDImage: UIImageView!
    @IBOutlet weak var NSAGDSingleName: UILabel!
    @IBOutlet weak var NSAGDName: UILabel!
    @IBOutlet weak var NSAGDHighest: UILabel!
    @IBOutlet weak var NSAGDPrice: UILabel!
    @IBOutlet weak var NSAGDRange: UILabel!
    
    @IBOutlet weak var NSAGDRangeImage: UIImageView!
    
    var model: BTCModel! {
        didSet{
            NSAGDImage.sd_setImage(with: URL.init(string: NSAGDbitCoinsImages[model.name]!), completed: nil)
            NSAGDSingleName.text = model.singleName
            NSAGDName.text = model.name
            NSAGDHighest.text = String.init(format: "$%.2f", model.highest)
            
            let range = model.range
            let color = range>=0 ? NSAGDupColor : NSAGDdownColor
            let rangePre = range>=0 ? "+" : ""
            
            NSAGDPrice.text = String.init(format: "$%.2f", model.price)
            NSAGDPrice.textColor = color
            NSAGDRange.text = rangePre + String.init(format: "%.2f", range) + "%"
            NSAGDRange.textColor = color
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
