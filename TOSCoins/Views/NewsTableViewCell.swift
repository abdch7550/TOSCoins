//
//  NewsTableViewCell.swift
//  TOSCoins
//
//  Created by mac on 2021/3/17.
//  Copyright © 2021 Chacky. All rights reserved.
//

import UIKit
import SDWebImage

class NewsTableViewCell: UITableViewCell {

    @IBOutlet weak var NSAGDImage: UIImageView!
    @IBOutlet weak var NSAGDTitle: UILabel!
    @IBOutlet weak var NSAGDDate: UILabel!
    
    var model: NewsModel!{
        didSet{
            if model.imageArray.count != 0{
                NSAGDImage.sd_setImage(with: URL.init(string: model.imageArray[0]), completed: nil)
            }
            
            NSAGDTitle.text = model.title
            NSAGDDate.text = model.date
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
