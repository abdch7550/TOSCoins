//
//  QuotesCollectionViewCell.swift
//  TOSCoins
//
//  Created by mac on 2021/3/17.
//  Copyright © 2021 Chacky. All rights reserved.
//

import UIKit

class QuotesCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var NSAGDImage: UIImageView!
    @IBOutlet weak var NSAGDSingleName: UILabel!
    @IBOutlet weak var NSAGDPrice: UILabel!
    @IBOutlet weak var NSAGDRange: UILabel!
    
    var model: BTCModel! {
        didSet{
            NSAGDImage.sd_setImage(with: URL.init(string: NSAGDbitCoinsImages[model.name]!), completed: nil)
            NSAGDSingleName.text = model.singleName
            
            let range = model.range
            let color = range>=0 ? NSAGDupColor : NSAGDdownColor
            let rangePre = range>=0 ? "+" : ""
            
            NSAGDPrice.text = String.init(format: "$%.2f", model.price)
            NSAGDPrice.textColor = color
            NSAGDRange.text = rangePre + String.init(format: "%.2f", range) + "%"
            NSAGDRange.textColor = color
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
