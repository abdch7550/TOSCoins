//
//  NewsModel.swift
//  TOSCoins
//
//  Created by mac on 2021/3/17.
//  Copyright © 2021 Chacky. All rights reserved.
//

import Foundation
import UIKit

class NewsModel:NSObject{
    
    @objc var title = String()
    @objc var date = String()
    @objc var timeInterval = Int()
    @objc var content = String()
    @objc var imageArray = [String]()
    @objc var source = String()
    
    override init() {
        //no
    }
    
    init(json:JSON){
        super.init()
        self.initWithJson(json: json)
    }
    
    func initWithJson(json:JSON){
        var NSAGDdics = [String:Any]()
        NSAGDdics["title"] = json["title"].stringValue
        
        let timeIntervals = json["timestamp"].intValue/1000
        NSAGDdics["timeInterval"] = timeIntervals
        NSAGDdics["date"] = NSAGDmyDateFormat.string(from: Date.init(timeIntervalSince1970: TimeInterval(timeIntervals)))
        
        NSAGDdics["content"] = json["content"].stringValue
        NSAGDdics["imageArray"] = (json["images"].arrayObject as! [String])
        NSAGDdics["source"] = json["source"].stringValue
        self.setValuesForKeys(NSAGDdics)
    }
}
