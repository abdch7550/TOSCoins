//
//  BTCModel.swift
//  TOSCoins
//
//  Created by mac on 2021/3/17.
//  Copyright © 2021 Chacky. All rights reserved.
//

import Foundation
import UIKit


class BTCModel:NSObject{
    
    @objc var name = String()
    @objc var singleName = String()
    @objc var price = Double()
    @objc var tradingVolume = Double()
    @objc var range = Double()
    @objc var rangePercent = String()
    @objc var highest = Double()
    @objc var lowest = Double()
    @objc var totalPrice = Double()
    @objc var historyHighest = Double()
    @objc var historyLowest = Double()
    
    init(json:JSON) {
        super.init()
        self.initWithJson(json: json)
    }
    
    func initWithJson(json:JSON){
        var NSAGDdic = [String:Any]()
        NSAGDdic["name"] = json["s"].stringValue
        NSAGDdic["singleName"] = json["S"].stringValue
        NSAGDdic["price"] = json["u"].doubleValue.truncate(places: 2)
        NSAGDdic["tradingVolume"] = json["a"].doubleValue.truncate(places: 2)
        
        
        let rangeDouble = json["c"].doubleValue
        NSAGDdic["range"] = rangeDouble.truncate(places: 4)
        NSAGDdic["rangePercent"] = (rangeDouble>=0 ? "+" : "") + String.init(format: "%.2f", rangeDouble*100) + "%"
        
        NSAGDdic["highest"] = json["h"].doubleValue.truncate(places: 2)
        NSAGDdic["lowest"] = json["l"].doubleValue.truncate(places: 2)
        NSAGDdic["totalPrice"] = json["m"].doubleValue.truncate(places: 2)
        NSAGDdic["historyHighest"] = json["ha"].doubleValue.truncate(places: 2)
        NSAGDdic["historyLowest"] = json["la"].doubleValue.truncate(places: 2)
        self.setValuesForKeys(NSAGDdic)
    }
    
}

