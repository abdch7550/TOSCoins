//
//  HistoryModel.swift
//  TOSCoins
//
//  Created by mac on 2021/3/17.
//  Copyright © 2021 Chacky. All rights reserved.
//

import Foundation
import UIKit


class HistoryModel:NSObject{
    
    @objc var timeInterval = Int()
    @objc var price = Double()
    @objc var tradingVolume = Double()
    @objc var allPrice = Double()
    
    init(json:JSON) {
        super.init()
        self.initWithJson(json: json)
    }
    
    func initWithJson(json:JSON){
        var NSAGDdic = [String:Any]()
        NSAGDdic["timeInterval"] = json["T"].intValue
        NSAGDdic["price"] = json["u"].doubleValue.truncate(places: 2)
        NSAGDdic["tradingVolume"] = json["a"].doubleValue.truncate(places: 2)
        NSAGDdic["allPrice"] = json["m"].doubleValue.truncate(places: 2)
        
        self.setValuesForKeys(NSAGDdic)
    }
    
}
