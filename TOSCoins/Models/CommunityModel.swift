//
//  DiscussModel.swift
//  TOSCoins
//
//  Created by mac on 2021/3/17.
//  Copyright © 2021 Chacky. All rights reserved.
//

import Foundation
import UIKit

class DiscussModel:NSObject{
    
    @objc var nick = String()
    @objc var portrait = String()
    @objc var date = String()
    @objc var content = String()
    @objc var image = String()
    @objc var tag = String()
    @objc var userID = String()
    
    @objc var title = String()
    @objc var support = 0
    
    @objc var comments = [CommunityCommentModel]()
    
    
    override init() {
        //Nothing
    }
    
    init(json: JSON){
        super.init()
        self.initWithJson(json: json)
    }
    
    func initWithJson(json: JSON){
        var NSAGDdics = [String:Any]()
        NSAGDdics["title"] = json["title"].stringValue
        NSAGDdics["nick"] = json["nick"].stringValue
        NSAGDdics["portrait"] = json["portrait"].stringValue
        NSAGDdics["date"] = json["date"].stringValue
        NSAGDdics["content"] = json["content"].stringValue
        NSAGDdics["image"] = json["image"].stringValue
        NSAGDdics["tag"] = json["tag"].stringValue
        NSAGDdics["userID"] = json["userID"].stringValue
        
        let jsonComments = json["comments"].arrayValue
        var NSAGDdicComments = [CommunityCommentModel]()
        if jsonComments.count != 0{
            for comment in jsonComments{
                NSAGDdicComments.append(CommunityCommentModel.init(json: comment))
            }
        }
        
        NSAGDdics["comments"] = NSAGDdicComments
        NSAGDdics["support"] = json["support"].intValue
        
        self.setValuesForKeys(NSAGDdics)
    }
}

func discussModelToJson(model: DiscussModel) -> JSON {
    var NSAGDdics = [String:Any]()
    NSAGDdics["title"] = model.title
    NSAGDdics["nick"] = model.nick
    NSAGDdics["portrait"] = model.portrait
    NSAGDdics["date"] = model.date
    NSAGDdics["content"] = model.content
    NSAGDdics["image"] = model.image
    NSAGDdics["tag"] = model.tag
    NSAGDdics["userID"] = model.userID
    
    let jsonComments = model.comments
    var NSAGDdicComments = [[String:Any]]()
    if jsonComments.count != 0{
        for comment in jsonComments{
            let commentArray = discussCommentModelToJson(model: comment).dictionaryObject
            NSAGDdicComments.append(commentArray!)
        }
    }
    
    NSAGDdics["comments"] = NSAGDdicComments
    
    NSAGDdics["support"] = model.support
    
    return JSON.init(NSAGDdics)
}
