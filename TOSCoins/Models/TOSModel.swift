//
//  TOSModel.swift
//  TOSCoins
//
//  Created by mac on 2021/3/17.
//  Copyright © 2021 Chacky. All rights reserved.
//

import Foundation
import UIKit

class TOSModel: NSObject {
    
    static let shard = TOSModel.init()
    
    var DiscussModel:JSON!{
        get{
            let path = URL.init(fileURLWithPath: NSTemporaryDirectory().appending("DiscussModel.json"))
            do {
                let data = try Data.init(contentsOf: path)
                let jsons = try JSON.init(data: data)
                
                return jsons
            } catch _ {
                return JSON()
            }
        }
    }
    
    class func changeDiscussModel(json:JSON){
            var topics = TOSModel.shard.DiscussModel.arrayValue
            if topics.contains(where: { (JSON) -> Bool in
                return JSON["tag"].stringValue == json["tag"].stringValue
            }){
                topics[topics.firstIndex(where: { (JSON) -> Bool in
                    return JSON["tag"].stringValue == json["tag"].stringValue
                })!] = json
            }
            else{
                topics.append(json)
            }
            let jsonStr = JSON.init(topics).rawString()
            let path = URL.init(fileURLWithPath: NSTemporaryDirectory().appending("DiscussModel.json"))
            do {
                try jsonStr?.data(using: .utf8)?.write(to: path)
                print("DiscussModel:\(TOSModel.shard.DiscussModel.arrayValue)")
                NotificationCenter.default.post(name: NSNotification.Name.init("modelChange"), object: nil)
            } catch let error {
                print(error)
            }
        }
        
    class func deleteDiscussModel(json:JSON){
        var topics = TOSModel.shard.DiscussModel.arrayValue
        if topics.contains(where: { (JSON) -> Bool in
            return JSON["tag"].stringValue == json["tag"].stringValue
            }){
                topics.remove(at: topics.firstIndex(where: { (JSON) -> Bool in
                return JSON["tag"].stringValue == json["tag"].stringValue
            })!)
        }
            
        let jsonStr = JSON.init(topics).rawString()
        let path = URL.init(fileURLWithPath: NSTemporaryDirectory().appending("DiscussModel.json"))
        do {
            try jsonStr?.data(using: .utf8)?.write(to: path)
            print(TOSModel.shard.DiscussModel.arrayValue)
            NotificationCenter.default.post(name: NSNotification.Name.init("modelChange"), object: nil)
        } catch let error {
            print(error)
        }
    }
    
        
    
}
