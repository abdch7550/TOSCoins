//
//  CommunityCommentModel.swift
//  TOSCoins
//
//  Created by mac on 2021/3/17.
//  Copyright © 2021 Chacky. All rights reserved.
//

import Foundation
import UIKit

class CommunityCommentModel:NSObject{
    
    @objc var nick = String()
    @objc var portrait = String()
    @objc var date = String()
    @objc var content = String()
    @objc var tag = String()
    @objc var userID = String()
    
    @objc var support = Int()
    
    override init() {
        //Nothing
    }
    
    init(json:JSON){
        super.init()
        self.initWithJson(json: json)
    }
    
    func initWithJson(json:JSON){
        var NSAGDdics = [String:Any]()
        NSAGDdics["nick"] = json["nick"].stringValue
        NSAGDdics["portrait"] = json["portrait"].stringValue
        NSAGDdics["date"] = json["date"].stringValue
        NSAGDdics["content"] = json["content"].stringValue
        NSAGDdics["tag"] = json["tag"].stringValue
        NSAGDdics["userID"] = json["userID"].stringValue
        
        NSAGDdics["support"] = json["support"].intValue
        
        
        self.setValuesForKeys(NSAGDdics)
    }
}


func discussCommentModelToJson(model:CommunityCommentModel) -> JSON {
    
    var NSAGDdics = [String:Any]()
    NSAGDdics["nick"] = model.nick
    NSAGDdics["portrait"] = model.portrait
    NSAGDdics["date"] = model.date
    NSAGDdics["content"] = model.content
    NSAGDdics["tag"] = model.tag
    NSAGDdics["userID"] = model.userID
    
    NSAGDdics["support"] = model.support
    
    return JSON(NSAGDdics)
}
