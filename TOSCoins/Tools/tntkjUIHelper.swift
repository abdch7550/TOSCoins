//
//  UIHelper.swift
//  TOSCoins
//
//  Created by mac on 2021/3/17.
//  Copyright © 2021 Chacky. All rights reserved.
//

import Foundation
import UIKit
import Reachability

let NSAGDmainjStoryboard = UIStoryboard.init(name: "Main", bundle: nil)

let NSAGDmainHeight = UIScreen.main.bounds.height
let NSAGDmainWidth = UIScreen.main.bounds.width

let NSAGDmyDateFormat:DateFormatter = {
    let date = DateFormatter()
    date.dateFormat = "yyyy.MM.dd HH:mm"
    return date
}()

let NSAGDmyTimeFormat:DateFormatter = {
    let date = DateFormatter()
    date.dateFormat = "HH:mm"
    return date
}()

let NSAGDcalendarDateFormat:DateFormatter = {
    let date = DateFormatter()
    date.dateFormat = "yyyy.MM.dd 08:30"
    return date
}()

func NSAGDisReachability()->Bool{
    let reac = Reachability.forInternetConnection()
    return ((reac?.isReachableViaWiFi())!||(reac?.isReachableViaWWAN())!)
}

func NSAGDsaveImage(currentImage: UIImage, persent: CGFloat, imageName: String) -> String{
    if let imageData = currentImage.jpegData(compressionQuality: persent) as NSData? {
            let fullPath = NSHomeDirectory().appending("/Documents/").appending(imageName)
            imageData.write(toFile: fullPath, atomically: true)
            print("fullPath=\(fullPath)")
            return fullPath
    }else{
        return "save image error."
    }
}

func NSAGDgetPortraitImage(imagePath : String)->UIImage{
    if let savedImage = UIImage(contentsOfFile: NSHomeDirectory().appending("/Documents/").appending(imagePath)) {
        return savedImage
    } else {
        return UIImage.init(named: "NP")!
    }
}

func NSAGDgetDiscussImage(imagePath : String)->UIImage{
    if let savedImage = UIImage(contentsOfFile: NSHomeDirectory().appending("/Documents/").appending(imagePath)) {
        return savedImage
    } else {
        return UIImage.init(named: "D1")!
    }
}


func NSAGDheightForLabel(text: String, font: UIFont, width: CGFloat) -> CGFloat{
    
    let label:UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: width, height: CGFloat.greatestFiniteMagnitude))
    label.numberOfLines = 0
    label.lineBreakMode = NSLineBreakMode.byWordWrapping
    label.font = font
    label.text = text
    label.sizeToFit()
    
    return label.frame.height
}


let NSAGDallBackgroundColor = NSAGDUIColorFromRGB(color_vaule: 0x0A0618)
let NSAGDtheamColor = NSAGDUIColorFromRGB(color_vaule: 0x14BB87)//2470A5 2D86C5

let NSAGDupColor = NSAGDUIColorFromRGB(color_vaule: 0xF17F3E)
let NSAGDdownColor = NSAGDUIColorFromRGB(color_vaule: 0x26A17B)

func NSAGDUIColorFromRGB(color_vaule: UInt64 , alpha: CGFloat = 1) -> UIColor {
    let redValue = CGFloat((color_vaule & 0xFF0000) >> 16)/255.0
    let greenValue = CGFloat((color_vaule & 0xFF00) >> 8)/255.0
    let blueValue = CGFloat(color_vaule & 0xFF)/255.0
    return UIColor(red: redValue, green: greenValue, blue: blueValue, alpha: alpha)
}

func NSAGDchangeImageColor(imageName: String, changeColor: UIColor) -> UIImage{
    let image = UIImage.init(named: imageName)
    UIGraphicsBeginImageContextWithOptions(image!.size, false, 0)
    changeColor.setFill()
    let bounds = CGRect.init(x: 0, y: 0, width: image!.size.width, height: image!.size.height)
    UIRectFill(bounds)
    image?.draw(in: bounds, blendMode: CGBlendMode.destinationIn, alpha: 1)
    let toImage = UIGraphicsGetImageFromCurrentImageContext()
    UIGraphicsEndImageContext()
    return toImage!
}

func NSAGDpushToSign(_ viewCon: UIViewController){
    let signAlert = UIAlertController.init(title: "", message: "Not signin yet, this function cannot be used ~", preferredStyle: .alert)
    let cancel = UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil)
    let toSign = UIAlertAction.init(title: "To Signin", style: .default) { (UIAlertAction) in
        viewCon.navigationController?.pushViewController(NSAGDmainjStoryboard.instantiateViewController(withIdentifier: "SigninViewController"), animated: true)
    }
    
    signAlert.addAction(cancel)
    signAlert.addAction(toSign)
    
    viewCon.present(signAlert, animated: true, completion: nil)
}

let NSAGDCoins = ["cardano","bitcoin-cash","eos","bitcoin","ethereum","tether","polkadot-new","chainlink","litecoin","binance-coin","stellar","uniswap","dogecoin","ripple","alt-estate","wrapped-bitcoin","aavenew","bitcoin-cash-sv","xplosive-ethereum"]

let NSAGDbitCoinsImages = ["cardano":"https://mifengcha.oss-cn-beijing.aliyuncs.com/static/coinInfo/cardano.png",
"bitcoin-cash":"https://mifengcha.oss-cn-beijing.aliyuncs.com/static/coinInfo/bitcoin-cash.png",
"eos":"https://mifengcha.oss-cn-beijing.aliyuncs.com/static/coinInfo/eos.png",
"bitcoin":"https://mifengcha.oss-cn-beijing.aliyuncs.com/static/coinInfo/bitcoin.png",
"ethereum":"https://mifengcha.oss-cn-beijing.aliyuncs.com/static/coinInfo/ethereum.png",
"tether":"https://mifengcha.oss-cn-beijing.aliyuncs.com/static/coinInfo/tether.png",
"polkadot-new":"https://mifengcha.oss-cn-beijing.aliyuncs.com/static/coinInfo/polkadot-new.png",
"chainlink":"https://mifengcha.oss-cn-beijing.aliyuncs.com/static/coinInfo/chainlink.png",
"litecoin":"https://mifengcha.oss-cn-beijing.aliyuncs.com/static/coinInfo/litecoin.png",
"binance-coin":"https://mifengcha.oss-cn-beijing.aliyuncs.com/static/coinInfo/binance-coin.png",
"stellar":"https://mifengcha.oss-cn-beijing.aliyuncs.com/static/stellar.png/coinInfo.png",
"uniswap":"https://mifengcha.oss-cn-beijing.aliyuncs.com/static/coinInfo/uniswap.png",
"dogecoin":"https://mifengcha.oss-cn-beijing.aliyuncs.com/static/dogecoin.png/coinInfo.png",
"alt-estate":"https://mifengcha.oss-cn-beijing.aliyuncs.com/static/coinInfo/alt-estate.jpg",
"wrapped-bitcoin":"https://mifengcha.oss-cn-beijing.aliyuncs.com/static/coinInfo/wrapped-bitcoin.png",
"ripple":"https://mifengcha.oss-cn-beijing.aliyuncs.com/static/coinInfo/ripple.png",
"aavenew":"https://mifengcha.oss-cn-beijing.aliyuncs.com/static/coinInfo/aavenew.png",
"bitcoin-cash-sv":"https://mifengcha.oss-cn-beijing.aliyuncs.com/static/coinInfo/bitcoin-cash-sv.png",
"xplosive-ethereum":"https://mifengcha.oss-cn-beijing.aliyuncs.com/static/coinInfo/xplosive-ethereum.jpeg"]

