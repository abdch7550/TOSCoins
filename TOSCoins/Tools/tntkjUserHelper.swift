//
//  UserHelper.swift
//  TOSCoins
//
//  Created by mac on 2021/3/17.
//  Copyright © 2021 Chacky. All rights reserved.
//

import Foundation
import UIKit

let NSAGDuserStand = UserDefaults.standard

let NSAGDuserStateKey = "userState"

let NSAGDuserNameKey = "userName"
let NSAGDuserPasswordKey = "userPassword"
let NSAGDuserIDKey = "userID"
let NSAGDuserPortraitKey = "userPortrait"

let NSAGDuserSupportKey = "mySupports"

let NSAGDuserCollectsKey = "myCollects"

let NSAGDuserInfosKey = "userInfoDic"
var NSAGDuserInfosValue = [
    [NSAGDuserNameKey:"276322",NSAGDuserPasswordKey:"123456",NSAGDuserPortraitKey:"UP",NSAGDuserIDKey:"0000001"]
]

func NSAGDgetMyCollects() -> [String]{
    var myCollects = [String]()
    if NSAGDuserStand.array(forKey: NSAGDuserCollectsKey) != nil{
        var allCollects = NSAGDuserStand.array(forKey: NSAGDuserCollectsKey) as! [[String:Any]]
        if allCollects.count != 0{
            allCollects = allCollects.filter { (NSAGDdic) -> Bool in
            return (NSAGDdic[NSAGDuserIDKey] as! String) == NSAGDuserStand.string(forKey: NSAGDuserIDKey)
            }
            if allCollects.count != 0{
                myCollects = allCollects[0][NSAGDuserCollectsKey] as! [String]
            }
        }
    }
    return myCollects
}


func saveMyCollects(collects: [String]){
    
    let NSAGDuserIDString = NSAGDuserStand.string(forKey: NSAGDuserIDKey)
    
    let myCollect = [NSAGDuserIDKey:NSAGDuserIDString as Any,NSAGDuserCollectsKey:collects]
    
    if NSAGDuserStand.array(forKey: NSAGDuserCollectsKey) != nil{
        
        var allCollects = NSAGDuserStand.array(forKey: NSAGDuserCollectsKey) as! [[String:Any]]
        let myCollects = allCollects.filter { (collect) -> Bool in
            return (collect[NSAGDuserIDKey] as! String) == NSAGDuserIDString
        }
        
        if myCollects.count != 0{
            allCollects[allCollects.firstIndex(where: { (collect) -> Bool in
                return (collect[NSAGDuserIDKey] as! String) == NSAGDuserIDString
            })!] = myCollect
        }
        else{
            allCollects.append(myCollect)
        }
        
        NSAGDuserStand.set(allCollects, forKey: NSAGDuserCollectsKey)
    }
    else{
        let allCollects = [myCollect]
        NSAGDuserStand.set(allCollects, forKey: NSAGDuserCollectsKey)
    }
}


func NSAGDgetMySupports() -> [String]{
    var mySupports = [String]()
    if NSAGDuserStand.array(forKey: NSAGDuserSupportKey) != nil{
        var allSupports = NSAGDuserStand.array(forKey: NSAGDuserSupportKey) as! [[String:Any]]
        if allSupports.count != 0{
            allSupports = allSupports.filter { (NSAGDdic) -> Bool in
            return (NSAGDdic[NSAGDuserIDKey] as! String) == NSAGDuserStand.string(forKey: NSAGDuserIDKey)
            }
            if allSupports.count != 0{
                mySupports = allSupports[0][NSAGDuserSupportKey] as! [String]
            }
        }
    }
    return mySupports
}


func NSAGDsaveMySupports(supports: [String]){
    
    let NSAGDuserIDString = NSAGDuserStand.string(forKey: NSAGDuserIDKey)
    
    let mySupport = [NSAGDuserIDKey:NSAGDuserIDString as Any,NSAGDuserSupportKey:supports]
    
    if NSAGDuserStand.array(forKey: NSAGDuserSupportKey) != nil{
        
        var allSupports = NSAGDuserStand.array(forKey: NSAGDuserSupportKey) as! [[String:Any]]
        let mySupports = allSupports.filter { (support) -> Bool in
            return (support[NSAGDuserIDKey] as! String) == NSAGDuserIDString
        }
        
        if mySupports.count != 0{
            allSupports[allSupports.firstIndex(where: { (support) -> Bool in
                return (support[NSAGDuserIDKey] as! String) == NSAGDuserIDString
            })!] = mySupport
        }
        else{
            allSupports.append(mySupport)
        }
        
        NSAGDuserStand.set(allSupports, forKey: NSAGDuserSupportKey)
    }
    else{
        let allSupports = [mySupport]
        NSAGDuserStand.set(allSupports, forKey: NSAGDuserSupportKey)
    }
}


func NSAGDreportClick(viewCon: UIViewController, modelTag: String){
    let NSAGDreports = NSAGDgetReports()
    
    if NSAGDreports.contains(modelTag){
        NSAGDHUDHelper.showString(withStatus: "The post has been reported. Please wait patiently for the result ~")
    }
    else{
        let nextView = NSAGDmainjStoryboard.instantiateViewController(withIdentifier: "ReportViewController") as! ReportViewController
        nextView.modelTag = modelTag
        viewCon.navigationController?.pushViewController(nextView, animated: true)
    }
}

func NSAGDgetReports() -> [String]{
    var NSAGDreports = [String]()
    guard let NSAGDmyReports = NSAGDuserStand.stringArray(forKey: "report") else { return NSAGDreports }
    NSAGDreports = NSAGDmyReports
    return NSAGDreports
}

func NSAGDsaveReports(modelTag: String){
    var NSAGDreports = [String]()
    guard let NSAGDmyReports = NSAGDuserStand.stringArray(forKey: "report") else {
        NSAGDreports.append(modelTag)
        NSAGDuserStand.set(NSAGDreports, forKey: "report")
        return
    }
    NSAGDreports = NSAGDmyReports
    NSAGDreports.append(modelTag)
    NSAGDuserStand.set(NSAGDreports, forKey: "report")
}


func NSAGDgetShield() -> [String]{
    var NSAGDshields = [String]()
    guard let NSAGDmyShields = NSAGDuserStand.stringArray(forKey: "shield") else { return NSAGDshields }
    NSAGDshields = NSAGDmyShields
    return NSAGDshields
}

func NSAGDsaveShield(str: String){
    var NSAGDshields = [String]()
    guard let NSAGDmyShields = NSAGDuserStand.stringArray(forKey: "shield") else {
        NSAGDshields.append(str)
        NSAGDuserStand.set(NSAGDshields, forKey: "shield")
        return
    }
    NSAGDshields = NSAGDmyShields
    NSAGDshields.append(str)
    NSAGDuserStand.set(NSAGDshields, forKey: "shield")
}
