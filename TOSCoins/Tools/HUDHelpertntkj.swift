//
//  SVProgressNSAGDHUDHelper.swift
//  TOSCoins
//
//  Created by mac on 2021/3/17.
//  Copyright © 2021 Chacky. All rights reserved.
//

import Foundation
import SVProgressHUD

var delayTime = 2.0

class NSAGDHUDHelper: NSObject {
    
    class func setMaskType(type:SVProgressHUDMaskType){
        SVProgressHUD.setDefaultMaskType(type)
    }
    
    class func show(){
        SVProgressHUD.show()
    }
    
    class func dismiss(){
        SVProgressHUD.dismiss()
    }
    
    class func dismiss(delay:TimeInterval) {
        SVProgressHUD.dismiss(withDelay: delay)
    }
    
    class func dismiss(complate:@escaping SVProgressHUDDismissCompletion){
        SVProgressHUD.dismiss(completion: complate)
    }
    
    class func dismiss(delay:TimeInterval,complate:@escaping SVProgressHUDDismissCompletion){
        SVProgressHUD.dismiss(withDelay: delay, completion: complate)
    }
    
    class func show(withStatus:String?){
        SVProgressHUD.show(withStatus: withStatus)
        SVProgressHUD.dismiss(withDelay: delayTime)
    }
    
    class func showInfo(withStatus: String?){
        SVProgressHUD.showInfo(withStatus: withStatus)
        SVProgressHUD.dismiss(withDelay: delayTime)
    }
    
    class func showInfo(withStatus: String?, withDelay: TimeInterval){
        SVProgressHUD.showInfo(withStatus: withStatus)
        SVProgressHUD.dismiss(withDelay: withDelay)
    }
    
    class func showProgress(progress: Float){
        SVProgressHUD.showProgress(progress)
    }
    
    class func showProgress(progress: Float, status: String?){
        SVProgressHUD.showProgress(progress, status: status)
    }
    
    class func showSuccess(withStatus: String?){
        SVProgressHUD.showSuccess(withStatus: withStatus)
        SVProgressHUD.dismiss(withDelay: delayTime)
    }
    
    class func showSuccess(withStatus: String?, withDelay: TimeInterval){
        SVProgressHUD.showSuccess(withStatus: withStatus)
        SVProgressHUD.dismiss(withDelay: withDelay)
    }
    
    class func showError(withStatus: String?){
        SVProgressHUD.showError(withStatus: withStatus)
        SVProgressHUD.dismiss(withDelay: delayTime)
    }
    
    class func showError(withStatus: String?, withDelay: TimeInterval){
        SVProgressHUD.showError(withStatus: withStatus)
        SVProgressHUD.dismiss(withDelay: withDelay)
    }
    
    class func show(image: UIImage, status: String?){
        SVProgressHUD.show(image, status: status)
        SVProgressHUD.dismiss(withDelay: delayTime)
    }
    
    class func showString(withStatus: String?){
        SVProgressHUD.show(UIImage(), status: "\(String(withStatus!)) \n\n")
        SVProgressHUD.dismiss(withDelay: delayTime)
    }
    
}
