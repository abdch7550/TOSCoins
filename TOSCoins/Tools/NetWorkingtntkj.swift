//
//  NetWorkingNSAGD.swift
//  TOSCoins
//
//  Created by mac on 2021/3/17.
//  Copyright © 2021 Chacky. All rights reserved.
//

import Foundation
import UIKit
import Alamofire

class NetWorkingNSAGD: NSObject {
    
        class func quatationRequest(requestType:HTTPMethod = .get ,parm: [String:Any],result: ((_ flag:Bool,_ value: Any) -> Void)?){

            var url = "http://hq.sinajs.cn/list="
            let strArray = parm["names"] as! [String]
            for item in strArray{
                url += ((parm["type"] as! String) == "normal" ) ? "\(item)," : "gb_\(item),"
            }
            
            print("the url : \(url),the parm : \(parm)")
            
            Alamofire.request(url, method:.get, parameters: nil).responseString { (str) in
                if str.result.isSuccess{
                    print(str.result.value!)
                    var Allstrs = str.result.value!.split(separator: ";")
                    Allstrs.remove(at: Allstrs.count-1)
                    var allArray = [[String]]()
                    for item in Allstrs{
                        let strs = String(item)
                        let name = strs.subString(from: strs.lastIndex(of: "_")!, to: strs.lastIndex(of: "=")!)
                        print("FutureName:\(name)")
                        let ste:Character = "\""
                        var dataStrs = strs.subString(from: strs.firstIndex(of: ste)!, to: strs.lastIndex(of: ste)!)
                        dataStrs = dataStrs.replacingOccurrences(of: "\"", with: "")
                        dataStrs = dataStrs.replacingOccurrences(of: ";", with: "")
                        var aData = dataStrs.split(separator: ",")
                        aData.append(String.SubSequence.init(name))
                        var strings = [String]()
                        for str in aData{
                            strings.append(String(str))
                        }
                        allArray.append(strings)
                    }
                    result!(true,allArray)
                    print(allArray)
                }
                else{
                    result!(false,"Data request failed")
                }
            }
        }
    
    class func BTCRequest(requestType:HTTPMethod = .get ,parm: [String:Any] = [String:Any](),result: ((_ flag:Bool,_ value: Any) -> Void)?){
        
        let key = "api_key=GAS1RPSL2J9LWNMBESWFSJXG5OPA2VF27LJK0TXU&"
        var url = "https://data.mifengcha.com/api/v3/"
        
        let parmType = parm["type"] as! String
        
        if parmType == "price"{
            let userCoins = parm["names"] as! [String]
            url += "price?"
            url += key
            url += "slug="
            if userCoins.count != 0{
                for i in 0...(userCoins.count-1){
                    if i == 0{
                        url += userCoins[i]
                    }else{
                        url += (","+userCoins[i])
                    }
                }
            }
        }
        else if parmType == "history"{
            let days = parm["days"] as! Int
            let interval = parm["interval"] as! String
            let coinName = parm["coin"] as! String
            url += "price/"
            url += "history?"
            url += key
            url += "slug=\(coinName)"
            let nowS = Int(Date().timeIntervalSince1970)
            let startS = String((nowS-days*60*24*60)*1000)
            url += "&start=\(startS)"
            let endS = String((nowS)*1000)
            url += "&end=\(endS)"
            url += "&interval=\(interval)"
        }
        else{
            let type = parmType == "normal" ? "briefs" : "articles"
            
            url += "\(type)?"//briefs  articles
            let size = parm["size"] as! String
            url += "size=\(size)&"
            url += key
            url += "locale=en_US"
        }
        
        
        print("the url : \(url),the parm : \(parm)")
        url = url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
        
        Alamofire.request(url, method:requestType, parameters: nil).responseJSON { (data) in
            if data.data != nil {
                let jsonData =  JSON.init(data.data!)
                print("the jsonData: \(jsonData)")
                if (jsonData["m"].stringValue.isEmpty) && (jsonData.array != nil){
                    result?(true,jsonData.array!)
                }else{
                    result?(false,"Network request failed")//jsonData["msg"].stringValue
                }
            }else{
                result!(false,"Network request failed")
            }
        }
    }
}
